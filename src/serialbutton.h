#ifndef SERIALBUTTON_H
#define SERIALBUTTON_H

#include <QPushButton>

class SerialButton : public QPushButton
{
    Q_OBJECT
public:
    SerialButton(int id);
    void action();
signals:
    void clicked(int id);
private:
    int id;
};

#endif // SERIALBUTTON_H
