#ifndef PAINTER_H
#define PAINTER_H

#include <QPainter>
#include <QLabel>
#include <QPicture>
#include <QWidget>

class Painter : public QWidget{
    Q_OBJECT
public:
    Painter(QWidget *parent, int width, int height);
    ~Painter();

    void setGrid(int x, int y);
public:
    void begin();
    void end();
    void drawPoint(int px, int py);
    void drawLine(int x1, int y1, int x2, int y2);
    int getWidth();
    int getHeight();
protected:
    QPainter* painter;
    QPicture* picture;
    QLabel* canvas;

    int width;
    int height;
    int gridX = 1;
    int gridY = 1;
};

#endif // PAINTER_H
