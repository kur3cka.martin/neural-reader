#ifndef PERCENTLABEL_H
#define PERCENTLABEL_H

#include <QLabel>

class SerialLabel:public QLabel
{
    Q_OBJECT
public:
    SerialLabel(int id);
public slots:
    void print(const double* output);
private:
    int id;
};

#endif // PERCENTLABEL_H
