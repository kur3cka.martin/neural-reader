#include <QDebug>
#include <QFile>
#include <QString>

#include "neuralnetwork.h"
#include "neuralnetworkcontroller.h"
#include "config.h"
#include <math.h>
#include <iostream>

/*********************************CONSTRUKTORS_AND_DESTRUCTORS**************************************/

/*
 * CONSTRUCTOR
 * Creates controller that handles manipulation with NeuralNetwork.
 */

NeuralNetworkController::NeuralNetworkController(config* conf):QWidget(NULL){
    this->height = conf->imgH;
    this->width = conf->imgW;
    this->conf = conf;

    if (conf->mode == config::ADAPTIVE){
        this->depth = conf->depth;
        nn = new NeuralNetwork(depth, conf->sizes);

        imageBuffer = new double*[width];
        for (int i=0;i<width;i++) {
            imageBuffer[i] = new double[height];
        }

        this->outSize = conf->sizes[depth-1];
        this->inSize = conf->sizes[0];
        this->input = new double[inSize];
        this->softmax = new double[outSize];
    }
}

NeuralNetworkController::~NeuralNetworkController(){
    for (int i=0;i<width;i++) {
        delete[] imageBuffer[i];
    }
    delete[] imageBuffer;
    delete[] input;
}

/*********************************BACKPROP_AND_CONFIGURATION_UTILITIES******************************/

/*
 * FUNCTION trainFromSet
 * Trains NeuralNetwork from data file specified in config file.
 */
int NeuralNetworkController::trainFromSet(){


    // Initialize arrays
    int* labels = new int[conf->trn];
    double* pictures = new double[conf->trn*(inSize)];

    // Read training data
    if(!loadSet(labels, pictures, conf->trn, conf->trainingData)) return 0;

    // Training:
    for (int i=0;i<conf->p[0];i++) {
        // Counter of succesfully distinguished numbers
        int successful = 0;
        // Alpha is a coeficient, that decrese learning rate.
        // As the number of succeses is higher alpha is smaller and weights are less changed.
        // That helps to better approximate seeked function.
        double alpha = 1;
        for (int j=0;j<conf->p[1];j++) {

            for (int k=0;k<conf->p[2];k++) {
                // Change learning rate of the Network according to alpha
                nn->learning_rate = conf->lr*alpha;

                // Pick random sample
                int pos = rand()%conf->trn;
                // Copy sample to input and run forwar propagation
                std::copy(pictures + pos*inSize, pictures + (pos+1)*inSize, input);
                nn->forward_propagation(input);

                // Create correct output vector
                double t[outSize] = {0};
                t[labels[pos]] = 1;

                // Find maximum of real output and compare it with correct digit
                double max = 0;
                int maxPos;
                for (int l=0;l<outSize;l++){
                    if(nn->y[l]>max){
                        max = nn->y[l];
                        maxPos = l;
                    }
                }
                if(maxPos == labels[pos])successful++;

                // Run backprop
                nn->back_propagation(t);
            }
            // After running backprop for samples of one mini-batch update weights
            nn->update_weights();
        }
        // Log result of one epoch and update alpha according to succeses
        qDebug() << successful << "/" << conf->p[1]*conf->p[2];
        alpha = 1-(successful/conf->p[1]*conf->p[2]);
    }
    delete [] labels;
    delete [] pictures;


    // Save weights configuration
    return saveConfiguration();
}

/*
 * FUNCTION testSet
 * Test NeuralNetwork on data file specified in config file.
 */
int NeuralNetworkController::testSet(){
    // Initialize arrays
    int* labels = new int[conf->ten];
    double* pictures = new double[conf->ten*(inSize)];

    // Read testing data
    if(!loadSet(labels, pictures, conf->ten, conf->testingData)) return 0;

    // Testing:
    int successful = 0;
    for (int i=0;i<conf->ten;i++) {
        // Copy sample to input and run forwar propagation
        std::copy(pictures + i*inSize, pictures + (i+1)*inSize, input);
        nn->forward_propagation(input);

        // Find maximum of real output and compare it with correct digit
        double max = 0;
        int maxPos;
        for (int l=0;l<outSize;l++){
            if(nn->y[l]>max){
                max = nn->y[l];
                maxPos = l;
            }
        }
        if(maxPos == labels[i])successful++;
        else if(conf->log){
            for (int y=0;y<conf->imgH;y++){
                for (int x=0;x<conf->imgW;x++){
                    std::cout << (input[y*conf->imgW+x]?"\033[1;31m":"\033[1;30m") << (char)24 << (char)24;
                }
                std::cout << "\n";
            }
            std::cout << "\n";
        }
    }

    qDebug() << successful << "/" << conf->ten;

    delete [] labels;
    delete [] pictures;

    return 1;
}

int NeuralNetworkController::loadSet(int* labels, double* pictures, int number, QString path){
    // Open CSV file with data
    qDebug() << "Opening  csv file.";
    QFile data(path);

    if (!data.open(QIODevice::ReadOnly)) {
        qDebug() << "CSV file couldn't be opened.";
        return 0;
    }

    // Read training data from CSV
    QTextStream in(&data);
    qDebug() << "Reading data CSV file.";
    for (int i=0;i<number;i++) {
        in >> labels[i];
        for (int j=0;j<inSize;j++) {
            char c;
            in >> c;
            in >> pictures[i*inSize+j];
        }
        preprocesing(pictures+i*inSize);
    }


    // Close file to prevent errors
    data.close();
    qDebug() << "Closing CSV file.";

    return 1;
}

int NeuralNetworkController::loadConfiguration(){
    // Open configuration file
    qDebug() << "Opening configuration file.";
    QFile configuration(conf->cfgPath);

    if (!configuration.open(QIODevice::ReadOnly)) {
        qDebug() << "Configuration file couldn't be opened.";
        return 0;
    }

    // Read configuration from file:
    QTextStream in(&configuration);
    qDebug() << "Writing configuration to file.";
    in >> depth;

    // Set architecture
    if(conf->sizes != NULL) delete[] conf->sizes;
    conf->sizes = new int[depth];
    for (int i=0;i<depth;i++) {
        in >> conf->sizes[i];
    }

    if (nn != NULL) delete nn;
    nn = new NeuralNetwork(depth, conf->sizes);
    if (imageBuffer != NULL){
        for (int i=0;i<width;i++) {
            delete[] imageBuffer[i];
        }
        delete[] imageBuffer;
    }

    imageBuffer = new double*[width];
    for (int i=0;i<width;i++) {
        imageBuffer[i] = new double[height];
    }

    this->outSize = conf->sizes[depth-1];
    this->inSize = conf->sizes[0];
    if (input != NULL) delete input;
    this->input = new double[inSize];
    if (softmax != NULL) delete softmax;
    this->softmax = new double[outSize];

    // Set configuration
    for (int l=1;l<depth;l++){
        for (int i=0;i<conf->sizes[l];i++) {
            in >> nn->b[l][i];
            for (int j=0; j<conf->sizes[l-1];j++) {
                in >> nn->w[l][i][j];
            }
        }
    }

    // Close file
    configuration.close();
    qDebug() << "Closing configuration file.";

    return 1;
}

int NeuralNetworkController::saveConfiguration(){
    // Open configuration file
    qDebug() << "Opening configuration file.";
    QFile configuration(conf->cfgPath);

    if (!configuration.open(QIODevice::WriteOnly)) {
        qDebug() << "Configuration file couldn't be opened.";
        return 0;
    }

    // Write configuration to file
    QTextStream out(&configuration);
    qDebug() << "Writing configuration to file.";
    out << depth << " ";
    for (int i=0;i<depth;i++) {
        out << conf->sizes[i] << " ";
    }
    out << "\n";

    for (int l=1;l<depth;l++){
        for (int i=0;i<conf->sizes[l];i++) {
            out << nn->b[l][i] << " ";
            for (int j=0; j<conf->sizes[l-1];j++) {
                out << nn->w[l][i][j] << " ";
            }
            out << "\n";
        }
        out << "\n";
    }

    // Close file
    configuration.close();
    qDebug() << "Closing configuration file.";

    return 1;
}

/*
 * IMAGE PREPROCESSING
 */

void NeuralNetworkController::merge(double* image){
    for(int x=0;x<width;x++){
        for(int y=0;y<height;y++){
            double sum = image[y*width+x];
            int c = 1;
            if(x>0){
                c++;
                sum += image[y*width+x-1];
            }
            if(y>0){
                c++;
                sum += image[(y-1)*width+x];
            }
            if(x<width-1){
                c++;
                sum += image[y*width+x+1];
            }
            if(y<height-1){
                c++;
                sum += image[(y+1)*width+x];
            }
            imageBuffer[x][y] = sum/c;
        }
    }
    for(int x=0;x<width;x++){
        for(int y=0;y<height;y++){
            image[y*width+x] = imageBuffer[x][y];
        }
    }
}

void NeuralNetworkController::normalize(double* image){
    double max = 0;
    for(int x=0;x<width;x++){
        for(int y=0;y<height;y++){
            max = max>image[y*width+x]?max:image[y*width+x];
        }
    }
    if (max == 0) return;
    for(int x=0;x<width;x++){
        for(int y=0;y<height;y++){
            image[y*width+x]/=max;
        }
    }
}

void NeuralNetworkController::preprocesing(double* image){
    int level = 2;
    for(int j=0;j<level;j++){
        merge(image);
    }
    normalize(image);
}

/******************************************SLOTS***************************************************/

void NeuralNetworkController::run(const double* in){
    std::copy(in, in+nn->size[0], input);
    preprocesing(input);
    nn->forward_propagation(input);

    std::copy(nn->y, nn->y+outSize, softmax);
    double sum;
    for (int i=0;i<outSize;i++){
        //softmax[i] = exp(softmax[i]);
        sum += softmax[i];
    }
    for (int i=0;i<outSize;i++){
        softmax[i] /= sum;
    }

    emit output(softmax);
}

void NeuralNetworkController::update(const int i){
    double t[nn->size[nn->depth-1]] = {0};
    t[i] = 1;
    nn->back_propagation(t);

    if (nn->not_updated >= conf->p[2]) {
        nn->update_weights();
    }
}
