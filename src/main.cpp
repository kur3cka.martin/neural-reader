#include <QApplication>
#include <iostream>
#include <QFile>
#include <QString>
#include <QStringList>
#include <QTextStream>
#include <QDebug>

#include "window.h"
#include "neuralnetwork.h"
#include "neuralnetworkcontroller.h"
#include "config.h"

int parseConfig(config* conf);
int runAdaptive(config* conf, int argc, char *argv[]);
int runActive(config* conf, int argc, char *argv[]);
int runTest(config* conf, int argc, char *argv[]);

/*
 * FUNCTION
 * Main function running whole application.
 */
int main(int argc, char *argv[])
{
    config* conf = new config;
    if (!parseConfig(conf)) return 0;
    if (!conf->mode) conf->mode = config::ACTIVE;

    if (conf->mode == config::ADAPTIVE) return runAdaptive(conf, argc, argv);
    else if (conf->mode == config::ACTIVE) return runActive(conf, argc, argv);
    else if (conf->mode == config::TEST) return runTest(conf, argc, argv);

    delete conf;

    return 0;
}


/*
 * FUNCTION
 * Used to read and parse data from config file.
 */
int parseConfig(config* conf) {
    QFile file(conf->configPath);
    if(!file.open(QIODevice::ReadOnly)) {
        qDebug() << "Couldn't open the config file.";
        return 0;
    }
    QTextStream in(&file);

    while (!in.atEnd()) {
        QString key;
        in >> key;

        if (key == "mode") {
            QString mode;
            in >> mode;

            if (mode == "adaptive") conf->mode = config::ADAPTIVE;
            if (mode == "active") conf->mode = config::ACTIVE;
            if (mode == "test") conf->mode = config::TEST;
            continue;
        }

        if (key == "training") {
            in >> conf->trainingData;
            continue;
        }

        if (key == "testing") {
            in >> conf->testingData;
            continue;
        }

        if (key == "depth") {
            in >> conf->depth;
            continue;
        }

        if (key == "img_width") {
            in >> conf->imgW;
            continue;
        }

        if (key == "img_height") {
            in >> conf->imgH;
            continue;
        }

        if (key == "packages") {
            in >> conf->p[0] >> conf->p[1] >> conf->p[2]; // number of epochs, size of epoch, size of minibatch
            continue;
        }

        if (key == "training_data_number") {
            in >> conf->trn;
            continue;
        }

        if (key == "testing_data_number") {
            in >> conf->ten;
            continue;
        }

        if (key == "learning_rate") {
            in >> conf->lr;
            continue;
        }

        if (key == "configuration") {
            in >> conf->cfgPath;
            continue;
        }

        if (key == "log") {
            in >> conf->log;
            continue;
        }

        if (key == "sizes") {
            if (conf->depth == 0) return 0;
            conf->sizes = new int[conf->depth];

            for (int i=0;i<conf->depth;i++){
                int size;
                in >> size;
                conf->sizes[i] = size;
            }

            continue;
        }
    }

    file.close();
    return 1;
}

/*
 * FUNCTION
 * Starts learning process.
 */
int runAdaptive(config* conf, int argc, char *argv[]){
    QApplication a(argc, argv);

    NeuralNetworkController ctrl(conf);
    return ctrl.trainFromSet();
}

/*
 * FUNCTION
 * Starts test.
 */
int runTest(config* conf, int argc, char *argv[]){
    QApplication a(argc, argv);

    NeuralNetworkController ctrl(conf);
    if (!ctrl.loadConfiguration()) return 0;
    return ctrl.testSet();
}

/*
 * FUNCTION
 * Starts GUI app.
 */
int runActive(config* conf, int argc, char *argv[]){
    QApplication a(argc, argv);

    NeuralNetworkController ctrl(conf);
    if (!ctrl.loadConfiguration()) return 0;

    Window w(&ctrl);
    w.show();

    return a.exec();
}

