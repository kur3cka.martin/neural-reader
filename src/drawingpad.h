#ifndef DRAWINGPAD_H
#define DRAWINGPAD_H

#include <QWidget>
#include "painter.h"

class DrawingPad : public Painter{
    Q_OBJECT
public:
    DrawingPad(QWidget* parent, int width, int height, int gx, int gy);
    ~DrawingPad();

    void redraw();

    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
private:
    int** arr;
    bool pressed = false;

    void merge(double *colors, int w, int h);
    void softmax(double *colors, int w, int h);
    void cut(double *colors, double *output, int w, int h, double margin);
    void preprocesing(double *colors, double *output, int w, int h, int level, double margin);
public slots:
    void updatePixel(const int x, const int y);
    void clean();
signals:
    void pictureUpdated(const double *arr);
};

#endif // DRAWINGPAD_H
