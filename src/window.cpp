#include "window.h"
#include "painter.h"
#include "drawingpad.h"
#include "neuralnetwork.h"
#include "neuralnetworkcontroller.h"
#include "seriallabel.h"
#include "serialbutton.h"
#include <QPushButton>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QGridLayout>
#include <QLabel>
#include <QFile>

Window::Window(NeuralNetworkController* ctrl):QMainWindow()
{
    // create app and define layout
    QWidget* app = new QWidget(this);
    QHBoxLayout* appLayout = new QHBoxLayout();
    app->setLayout(appLayout);


    // create control panel
    QVBoxLayout* controlLayout = new QVBoxLayout();

    QPushButton* restart = new QPushButton("CLEAR DISPLAY");
    controlLayout->addWidget(restart);

    QGridLayout* buttonBoxLayout = new QGridLayout();
    for(int i=0;i<10;i++){
        SerialLabel* l = new SerialLabel(i);
        SerialButton* b = new SerialButton(i);
        connect(ctrl, SIGNAL(output(const double*)), l, SLOT(print(const double*)));
        connect(b, &QPushButton::clicked, [b]() {
            b->action();
        });
        connect(b, SIGNAL(clicked(int)), ctrl, SLOT(update(int)));
        buttonBoxLayout->addWidget(b,i,0);
        buttonBoxLayout->addWidget(l,i,1);

    }
    controlLayout->addLayout(buttonBoxLayout);

    // create drawing panel
    appLayout->addLayout(controlLayout);

    DrawingPad* p = new DrawingPad(NULL, 600, 600, 28, 28);

    appLayout->addWidget(p);

    connect(restart, SIGNAL(clicked(bool)), p, SLOT(clean()));
    connect(p, SIGNAL(pictureUpdated(const double*)), ctrl, SLOT(run(const double*)));

    this->setCentralWidget(app);
}

/*
 * Set program variables
 */
void Window::init(){
    setWindowTitle("NN Reader");
}


