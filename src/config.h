#ifndef CONFIG_H
#define CONFIG_H

/*
 * STRUCTURE
 * Halds config information as mode or paths.
 */
struct config{
    int mode;
    QString configPath = QString("config.dat");
    QString trainingData;
    QString testingData;
    QString cfgPath;
    int depth = 0;
    int* sizes = NULL;

    int imgW;
    int imgH;

    int p[3];
    int trn;
    int ten;

    double lr;

    int log = 0;

    const static int ADAPTIVE = 100;
    const static int ACTIVE = 101;
    const static int TEST = 102;

    ~config(){
        if (sizes != NULL) {
            delete sizes;
        }
    }
};

#endif // CONFIG_H
