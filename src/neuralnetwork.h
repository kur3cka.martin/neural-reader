#ifndef NEURALNETWORK_H
#define NEURALNETWORK_H

class NeuralNetwork
{
    friend class NeuralNetworkController;
public:
    NeuralNetwork(int depth, const int* size);
    ~NeuralNetwork();
private:
    int not_updated = 0; // Number of iteration of backpropagation without updationg weights.
    double learning_rate; // Coeficicient that decribe learning speed. High speed gets accurancy lower.

    int depth; // Number of network layers
    int* size; // Array containing size of each layer

    double*** w; // Array of weight matrices. w[l][i][j] describe connection between neuron i in layer l and neuron j in layer l-1
    double** b; // Array of biases vectors. b[l][i] describe bias of neuron i in layer l.
    double*** w_change; // Sum of weigth changes in not updated iterations of backprop
    double** b_change; // Sum of bias changes in not updated iterations of backprop
    double** z; // Array of total inner potentials in neurons
    double** a; // Array of activation values of neurons
    double** a_d; // Array of derivatives of activation values of neuons.
    double** delta; // Array of partial error values delta of neurons.

    double* y; // Pointer to the output of network
private:
    void init_arrays();
    void destroy_arrays();
    void init_weights(double min, double max);

    // All core functions are private because we want just controller to reach them.
    void forward_propagation(const double* in);
    void back_propagation(const double* t);
    void update_weights();

    static double SIGMOID_FN(double z);
    static double SIGMOID_FND(double z);
    static double R_NUM(double min, double max);
    static double R_NUM();
    static double CVAD_COST_FND(const double& a, const double& a_d, const double& t);
};

#endif // NEURALNETWORK_H
