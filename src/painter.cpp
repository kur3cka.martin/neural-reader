#include "painter.h"

Painter::Painter(QWidget *parent, int width, int height):QWidget(parent){
    this->width = width;
    this->height = height;

    picture = new QPicture();
    painter = new QPainter();
    canvas = new QLabel(this);
    canvas->setPicture(*picture);
    this->setMinimumSize(width, height);
    canvas->setMinimumSize(width, height);

    setGrid(width/2,height/2);

    begin();
    end();
}

Painter::~Painter(){
    delete picture;
    delete painter;
    delete canvas;
}

void Painter::drawLine(int x1, int y1, int x2, int y2){
    painter->drawLine(x1, y1, x2, y2);
}

void Painter::drawPoint(int px, int py){
    int x1 = (int)((width/(float)gridX)*px);
    int y1 = (int)((height/(float)gridY)*py);
    int x2 = (int)((width/(float)gridX)*(px+1))-1;
    int y2 = (int)((height/(float)gridY)*(py+1))-1;

    painter->fillRect(QRect(x1, y1, x2-x1, y2-y1), Qt::white);
}

void Painter::setGrid(int x, int y){
    gridX = x;
    gridY = y;
}

void Painter::begin(){
    painter->begin(picture);
    int w = width - 1;
    int h = height - 1;
    drawLine(0,0,0,h);
    drawLine(w,0,w,h);
    drawLine(0,0,w,0);
    drawLine(0,h,w,h);
}

void Painter::end(){
    painter->end();
}

int Painter::getHeight(){
    return height;
}

int Painter::getWidth(){
    return width;
}
