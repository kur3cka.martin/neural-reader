#include "serialbutton.h"
#include <QDebug>

SerialButton::SerialButton(int id):QPushButton(NULL)
{
    setText(QString::number(id));
    this->id = id;
}

void SerialButton::action(){
    emit clicked(id);
}
