#ifndef NEURALNETWORKCONTROLLER_H
#define NEURALNETWORKCONTROLLER_H

#include "neuralnetwork.h"
#include <QWidget>
#include <QString>
#include "config.h"

/*
 * CLASS NeuralNetworkController
 * Used to handle manipulation with NeuralNetwork.
 *
 */
class NeuralNetworkController : public QWidget{
    Q_OBJECT
public:
    NeuralNetworkController(config* conf);
    ~NeuralNetworkController();

private:
    config* conf;

    /****************NETOWRK***************************/
    NeuralNetwork* nn = NULL;
    double* input = NULL;
    double* softmax = NULL; // Stores softmax values of Network output
    int inSize;
    int outSize;
    int depth;

    /****************IMAGE*****************************/
    int width;
    int height;
    double** imageBuffer = NULL;
public:
    int loadConfiguration();
    int saveConfiguration();
    int trainFromSet();
    int testSet();
    int loadSet(int* labels, double* pictures, int number, QString path);

    void preprocesing(double* image);
    void normalize(double* image);
    void merge(double* image);
public slots:
    void run(const double* in);
    void update(const int i);
signals:
    void output(const double* out);
};

#endif // NEURALNETWORKCONTROLLER_H

