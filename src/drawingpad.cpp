#include "drawingpad.h"
#include <QMouseEvent>
#include <QDebug>

DrawingPad::DrawingPad(QWidget *parent, int width, int height, int gx, int gy):Painter(parent, width, height){
    setGrid(gx, gy);
    arr = new int*[width];
    for(int i=0;i<width;i++){
        arr[i] = new int[height];
        for(int j=0;j<height;j++){
            arr[i][j] = 0;
        }
    }
}

DrawingPad::~DrawingPad(){
    for(int i=0;i<width;i++){
        delete[] arr[i];
    }
    delete[] arr;
}

void DrawingPad::redraw(){
    begin();
    double output[gridX*gridY];
    for(int i=0;i<gridX;i++){
        for(int j=0;j<gridY;j++){
            if(arr[i][j])drawPoint(i, j);
            output[i+j*gridX] = arr[i][j];
        }
    }
    end();
    canvas->update();
    emit pictureUpdated(output);
}

void DrawingPad::updatePixel(int x, int y){
    int px = x/(width/gridX);
    int py = y/(height/gridY);
    if(px<0 || py<0 || px>=gridX || py>=gridY)return;
    if(!arr[px][py]){
        arr[px][py] = 1;
        redraw();
    }
}

void DrawingPad::clean(){
    for(int i=0;i<gridX;i++){
        for(int j=0;j<gridY;j++){
            arr[i][j] = 0;
        }
    }
    redraw();
}

void DrawingPad::mousePressEvent( QMouseEvent* ev )
{
    pressed = true;
    updatePixel(ev->x(), ev->y());
}

void DrawingPad::mouseReleaseEvent( QMouseEvent* ev )
{
    pressed = false;
}

void DrawingPad::mouseMoveEvent( QMouseEvent* ev )
{
    if(pressed)updatePixel(ev->x(), ev->y());
}
