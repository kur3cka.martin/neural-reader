#include "seriallabel.h"

SerialLabel::SerialLabel(int id):QLabel(NULL)
{
    this->setText("0%");
    this->id = id;
}

void SerialLabel::print(const double* output){
    setText(QString::number((int)(output[id]*10000)/100.).append('%'));
    bool biggest = true;
    for(int i=0;i<10;i++){
        if(output[i]>output[id])biggest = false;
    }
    if(biggest)setStyleSheet("color: green");
    else setStyleSheet("color: white");
}
