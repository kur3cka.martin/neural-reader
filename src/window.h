#ifndef WINDOW_H
#define WINDOW_H

#include <QMainWindow>
#include "neuralnetworkcontroller.h"

class Window : public QMainWindow
{
    Q_OBJECT
public:
    Window(NeuralNetworkController* ctrl);
public:
    void init();
};

#endif // WINDOW_H
