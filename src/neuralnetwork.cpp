#include "neuralnetwork.h"
#include <time.h>
#include <stdlib.h>
#include <QDebug>
#include <QDir>
#include <iostream>
#include <fstream>

/*********************************CONSTRUKTORS_AND_DESTRUCTORS**************************************/

/*
 * CONSTRUCTOR --- NeuralNetwork
 * Creates NeuralNetwork with given architecture.
 * Connected to Controller might by trained from CSV file or might read it's configuration from file.
 */
NeuralNetwork::NeuralNetwork(int depth, const int* size)
{
    this->depth = depth;
    this->size = new int[depth];
    std::copy(size, size+depth, this->size);

    init_arrays();
    init_weights(0.001, 0.01);

    learning_rate = 0.1;
}

/*
 * DESTRUCTOR --- ~NeuralNetwork
 * Destroyes instance of NeuralNetowrk.
 */
NeuralNetwork::~NeuralNetwork(){
    destroy_arrays();
}

/************************************ARRAY_HANDLERS*************************************************/

/*
 * FUNCTION --- init_arrays
 * Initialize all necesary arrays.
 */
void NeuralNetwork::init_arrays(){
    b = new double*[depth];
    b_change = new double*[depth];
    w = new double**[depth];
    w_change = new double**[depth];
    a = new double*[depth];
    a_d = new double*[depth];
    delta = new double*[depth];
    z = new double*[depth];

    a[0] = new double[size[0]];
    for (int i=1;i<depth;i++) {
        b[i] = new double[size[i]];
        b_change[i] = new double[size[i]];
        z[i] = new double[size[i]];
        a[i] = new double[size[i]];
        delta[i] = new double[size[i]];
        a_d[i] = new double[size[i]];
        w[i] = new double*[size[i]];
        w_change[i] = new double*[size[i]];

        for (int j=0;j<size[i];j++) {
            w[i][j] = new double[size[i-1]];
            w_change[i][j] = new double[size[i-1]];
        }
    }

    y = a[depth-1];
}

/*
 * FUNCTION --- destroy_arrays
 * Destroyes all esed dynamic arrays.
 */
void NeuralNetwork::destroy_arrays(){
    delete[] a[0];
    for(int i=1;i<depth;i++){
        delete[] b[i];
        delete[] b_change[i];
        delete[] z[i];
        delete[] a[i];
        delete[] delta[i];
        delete[] a_d[i];

        for (int j=0;j<size[i];j++) {
            delete[] w[i][j];
            delete[] w_change[i][j];
        }

        delete[] w[i];
        delete[] w_change[i];
    }

    delete[] b;
    delete[] b_change;
    delete[] a;
    delete[] a_d;
    delete[] delta;
    delete[] z;
    delete[] w;
    delete[] w_change;
}

/*
 * FUNCTION --- init_weights
 * Initialize weights and biases with random values from given interval.
 */
void NeuralNetwork::init_weights(double min, double max){
    srand(time(NULL));

    for (int i=1;i<depth;i++) {

        for (int j=0;j<size[i];j++) {

            b[i][j] = R_NUM(min, max);
            b_change[i][j] = 0;

            for (int k=0;k<size[i-1];k++) {
                w[i][j][k] = R_NUM(min, max);
                w_change[i][j][k] = 0;
            }   
        }
    }
}

/**************************************CORE_FUNCTIONS***********************************************/

/*
 * FUNCTION --- forward_propagation
 * Takes input vector of network and computes all activation values and corespondig derivatives.
 * Vector of activations in last layer represents output of whole NeuralNetwork
 */
void NeuralNetwork::forward_propagation(const double* in){
    std::copy(in, in+size[0], a[0]);

    for (int l=1;l<depth;l++) {

        for (int j=0;j<size[l];j++) {
            z[l][j] = b[l][j];

            for (int i=0;i<size[l-1];i++) {\
                z[l][j] += w[l][j][i] * a[l-1][i];
            }

            a[l][j] = SIGMOID_FN(z[l][j]);
            a_d[l][j] = SIGMOID_FND(z[l][j]);
        }
    }
}

/*
 * FUNCTION --- back_propagation
 * Takes correct output T and compare it with real output of network.
 * Using Cvadratic cost fuctions then computes delta values in each neuron and appropriat weight changes.
 */
void NeuralNetwork::back_propagation(const double* t){
    for (int i=0;i<size[depth-1];i++) {
        delta[depth-1][i] = CVAD_COST_FND(a[depth-1][i], a_d[depth-1][i], t[i]);
    }

    for(int l=depth-2;l>0;l--) {

        for(int i=0;i<size[l];i++) {
            delta[l][i] = 0;
            for(int j=0;j<size[l+1];j++){
                delta[l][i] += w[l+1][j][i]*delta[l+1][j];
            }
            delta[l][i] *= a_d[l][i];
        }
    }

    for(int l=1;l<depth;l++){
        for(int i=0;i<size[l];i++){
            b_change[l][i] -= delta[l][i];
            for(int j=0;j<size[l-1];j++){
                w_change[l][i][j] -= delta[l][i]*a[l-1][j];
            }
        }
    }

    not_updated++;
}

/*
 * FUNCTION --- update_weights
 * Change values of weights and biases by average change values in not updated cycles of backprop.
 */
void NeuralNetwork::update_weights(){
    for (int l=1;l<depth;l++) {

        for (int i=0;i<size[l];i++) {
            b[l][i] += (learning_rate*b_change[l][i])/not_updated;
            b_change[l][i] = 0;
            //QDebug deb = qDebug();
            for (int j=0;j<size[l-1];j++) {
                w[l][i][j] += (learning_rate*w_change[l][i][j])/not_updated;
                //if(l==1)deb << w[l][i][j];
                w_change[l][i][j] = 0;
            }
        }
    }

    not_updated = 0;
}

/************************************MATH_FUNCTIONS_DEFINITIONS*************************************/

/*
 * FUNCTION --- R_NUM
 * Generates random number in given interval.
 */
double NeuralNetwork::R_NUM(double min, double max){
    return ((rand()%10000)/(double)9999)*(max-min)+min;
}

/*
 * FUNCTION --- R_NUM
 * Generates random number in interval [0;1].
 */
double NeuralNetwork::R_NUM(){
    return R_NUM(0,1);
}

/*
 * FUNCTION --- CVAD_COST_FND
 * Computes value of derivative of Cvadratic cost function.
 */
double NeuralNetwork::CVAD_COST_FND(const double& a, const double& a_d, const double& t){
    return (a-t)*a_d;
}

/*
 * FUNCTION --- SIGMOID_FN
 * Computes value of sigmoid function.
 */
double NeuralNetwork::SIGMOID_FN(double z){
    return (double)1.0/(1+exp(-z));
}

/*
 * FUNCTION --- SIGMOID_FND
 * Computes value of derivative of sigmoid function.
 * Uses fact that sig' = sig*(1-sig).
 */
double NeuralNetwork::SIGMOID_FND(double z){
    return SIGMOID_FN(z)*(1-SIGMOID_FN(z));
}
