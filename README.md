Before running programme it must be compiled. Code is created with c++11 standard and uses QT library therefore it's necesarry to have this library installed. The programme was written using QT 5.8 but it should be also possible to use lower versions.

All cpp scripts and header files are playced in folder src. Main folder contains files mnist_train.csv and mnist_test.csv. Both these files contain samples of handwriteen digits with resolution 28x28 pixels. Each line is compound of one integer 0-9 as a label and 784 integers of grayscale color values of pixel in range 0-255.

Another file in main folder is config.dat. It contains setting of whole programme. Mode might have values adaptive - learn from specified csv file, test - test network on data from specified file or actve - read user input in GUI.
After learning process configuration is saved to file specified by field configuration. Same file is used to load configuration in testing and active mode.
The field packages contans number of epochs, size of epoch and size of mini-batches during the adaptive mode.

All these files must be in the same folder as executed executable file.